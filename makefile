BIN=bin
SRC=src
BOOK_NAME=template

EPUBTOOL=foliate
PDFTOOL=evince
SEP=/

epub: $(SRC)$(SEP)*.md
	pandoc -s -t epub --epub-cover-image=res/cover.png --metadata="res/meta.xml" $(SRC)$(SEP)*.md -o $(BIN)$(SEP)$(BOOK_NAME).epub && $(EPUBTOOL) $(BIN)$(SEP)$(BOOK_NAME).epub

html: $(SRC)$(SEP)*.md
	pandoc -t html $(SRC)$(SEP)*.md -o $(BIN)$(SEP)$(BOOK_NAME).html && $(EPUBTOOL) $(BIN)$(SEP)$(BOOK_NAME).html

pdf: $(SRC)/*.md
	pandoc -s -t pdf --pdf-engine=wkhtmltopdf --metadata title=$(BOOK_NAME) $(SRC)/*.md -o $(BIN)/$(BOOK_NAME).pdf && $(PDFTOOL) $(BIN)$(SEP)$(BOOK_NAME).pdf

.PHONY: clean

clean:
	rm -fr $(BIN)$(SEP)*
